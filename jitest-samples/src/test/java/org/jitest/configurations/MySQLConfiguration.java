package org.jitest.configurations;

import org.jitest.annotations.ConfigurationName;
import org.jitest.annotations.Database;
import org.jitest.annotations.WebServer;

/**
 * Configuration sample using a MySQL database.
 *
 * @author Francisco Cortes Gamez.
 */
@Database(driver = "com.mysql.jdbc.Driver",
        url = "http://localhost:mysql:1111",
        userName = "peter",
        password = "walker",
        createSchemaScript = "/mysql/create-schema.sql",
        updateSchemaScript = "/mysql/update-schema.sql",
        cleanupSchemaScript = "/mysql/cleanup-schema.sql",
        deleteSchemaScript = "/mysql/delete-schema.sql")
@WebServer(host = "hostname",
        userName = "user",
        password = "pass",
        startCommand = "startup.bat",
        stopCommand = "shutdown.bat")
@ConfigurationName("MySQL")
public class MySQLConfiguration {
}
