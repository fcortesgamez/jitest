package org.jitest.configurations;

import org.jitest.annotations.ConfigurationName;
import org.jitest.annotations.Database;

/**
 * Configuration sample using a HSSQL database.
 *
 * @author Francisco Cortes Gamez.
 */
@Database(driver = "org.hsqldb.jdbcDriver",
        url = "jdbc:hsqldb:file:hsqldb/testdb;hsqldb.write_delay=false",
        userName = "sa",
        password = "",
        createSchemaScript = "hssql/create-schema.sql",
        updateSchemaScript = "hssql/update-schema.sql",
        cleanupSchemaScript = "hssql/cleanup-schema.sql",
        deleteSchemaScript = "hssql/delete-schema.sql")
@ConfigurationName("HSSQL")
public class HSSQLConfiguration {
}
