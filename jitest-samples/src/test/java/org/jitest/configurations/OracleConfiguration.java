package org.jitest.configurations;

import org.jitest.annotations.ConfigurationName;
import org.jitest.annotations.Database;
import org.jitest.annotations.WebServer;

/**
 * Configuration sample using a Oracle database.
 *
 * @author Francisco Cortes Gamez.
 */
@Database(driver = "oracle.jdbc.OracleDriver",
        url = "http://localhost:oracle:2222",
        userName = "fcortes",
        password = "fcortes",
        createSchemaScript = "/oracle/create-schema.sql",
        updateSchemaScript = "/oracle/update-schema.sql",
        cleanupSchemaScript = "/oracle/cleanup-schema.sql",
        deleteSchemaScript = "/oracle/delete-schema.sql")
@WebServer(host = "hostname",
        userName = "user",
        password = "pass",
        startCommand = "start.bat",
        stopCommand = "stop.bat")
@ConfigurationName("Oracle")
public class OracleConfiguration {
}
