package org.jitest.samples;

import org.jitest.annotations.Configurations;
import org.jitest.configurations.HSSQLConfiguration;
import org.jitest.runner.ITestSuite;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;

import static java.sql.DriverManager.getConnection;
import static java.sql.ResultSet.CONCUR_UPDATABLE;
import static java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration tests {@link org.jitest.samples.SimpleSample}.
 *
 * @author Francisco Cortes Gamez.
 */
// TODO: Probably a 'system property' should be used to specify which configuration(s) to use then overriding the ones
// specified in the @Configurations
@RunWith(ITestSuite.class)
//@Configurations({MySQLConfiguration.class, OracleConfiguration.class})
@Configurations({HSSQLConfiguration.class})
public class SimpleSampleTest {

    @Test
    public void testGetSum() throws Exception {
        System.out.println("Running testGetSum");
        assertThat(new SimpleSample().getSum(7, 7), is(14));
        assertThat(new SimpleSample().getSum(2, 4), is(6));
        System.out.println("Running testGetSum done");
    }

    @Test
    public void testGetSumAgain() throws Exception {
        System.out.println("Running testGetSumAgain");
        assertThat(new SimpleSample().getSum(1, 1), is(2));
        assertThat(new SimpleSample().getSum(6, 1), is(7));
        System.out.println("Running testGetSumAgain done");
    }

    @Test
    public void testDatabase() throws Exception {
        try (Connection conn = getConnection("jdbc:hsqldb:file:hsqldb/testdb;hsqldb.write_delay=false", "sa", "");
             Statement statement = conn.createStatement(TYPE_SCROLL_INSENSITIVE, CONCUR_UPDATABLE);) {

            // Note: This is here just for testing purposes, but I can use it as a starting point to find out how to
            // very smartly delete all tables from a database schema without the need to supply the deletion schema
            DatabaseMetaData connMetaData = conn.getMetaData();
            ResultSet tablesResultSet = connMetaData.getTables(null, null, "SAMPLE%", null);
            tablesResultSet.beforeFirst();
            while (tablesResultSet.next()) {
                System.out.println(String.format("%s", tablesResultSet.getString(3)));
            }

            ResultSet resultSet = statement.executeQuery("SELECT * FROM SAMPLE_TABLE_ONE");
            resultSet.beforeFirst();
            resultSet.next();
            assertThat(resultSet.getInt(1), is(1));
            assertThat(resultSet.getString(2), is("Francisco"));
            resultSet.next();
            assertThat(resultSet.getInt(1), is(2));
            assertThat(resultSet.getString(2), is("Peter"));
        }
    }
}
