package org.jitest.samples;

/**
 * Just a very simple class.
 *
 * @author Francisco Cortes Gamez
 */
public class SimpleSample {

    public int getSum(int x, int y) {
        return x + y;
    }
}
