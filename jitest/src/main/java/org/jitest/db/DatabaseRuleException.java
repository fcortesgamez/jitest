package org.jitest.db;

import org.jitest.core.JITestException;

/**
 * Exception to throw when something goes wrong setting up or cleaning up a database at a test suite or test level.
 *
 * @author Francisco Cortes Gamez.
 */
public class DatabaseRuleException extends JITestException {

    public DatabaseRuleException(String message) {
        super(message);
    }

    public DatabaseRuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseRuleException(Throwable cause) {
        super(cause);
    }
}
