package org.jitest.db;

import org.jitest.core.JITestRule;

/**
 * Generic interface which represents a handle to operate with any potential database needed by an integration test.
 * It has all the methods needed to ensure a database can be put in place and populate with some data before an
 * integration test can use it.
 *
 * @author Francisco Cortes Gamez.
 */
public interface DatabaseRule extends JITestRule {

    /**
     * Set the database driver.
     *
     * @param driver The database driver.
     * @return The database itself.
     */
    DatabaseRule setDriver(String driver);

    /**
     * Set the database URL.
     *
     * @param url The database URL.
     * @return The database itself.
     */
    DatabaseRule setUrl(String url);

    /**
     * Set the database user name.
     *
     * @param userName The database user name.
     * @return The database itself.
     */
    DatabaseRule setUsername(String userName);

    /**
     * Set the database password.
     *
     * @param password The database password.
     * @return The database itself.
     */
    DatabaseRule setPassword(String password);

    /**
     * Set the script used to create the schema.
     *
     * @param createSchemaScript The script used to create the schema.
     * @return The database itself.
     */
    DatabaseRule setCreateSchemaScript(String createSchemaScript);

    /**
     * Set the script used to update the schema.
     *
     * @param updateSchemaScript The script used to update the schema.
     * @return The database itself.
     */
    DatabaseRule setUpdateSchemaScript(String updateSchemaScript);

    /**
     * Set the script used to clean up the schema.
     *
     * @param cleanupSchemaScript The script used to clean up the schema.
     * @return The database itself.
     */
    DatabaseRule setCleanupSchemaScript(String cleanupSchemaScript);

    /**
     * Set the script used to delete the schema.
     *
     * @param deleteSchemaScript The script used to delete the schema.
     * @return The database itself.
     */
    DatabaseRule setDeleteSchemaScript(String deleteSchemaScript);
}
