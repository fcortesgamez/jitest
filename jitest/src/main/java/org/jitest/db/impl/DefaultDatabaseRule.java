package org.jitest.db.impl;

import org.jitest.db.DatabaseRule;
import org.jitest.db.DatabaseRuleException;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.IOException;
import java.io.Reader;
import java.sql.*;

import static java.sql.DriverManager.getConnection;
import static org.apache.ibatis.io.Resources.getResourceAsReader;

/**
 * Implementation of {@code Database} with support for the generic expected common functionality like for instance
 * getting/setting the connection details, SQL scripts to create and update the schema.
 *
 * @author Francisco Cortes Gamez.
 */
public class DefaultDatabaseRule implements DatabaseRule {

    private String driver;
    private String url;
    private String userName;
    private String password;
    private String createSchemaScript;
    private String updateSchemaScript;
    private String cleanupSchemaScript;
    private String deleteSchemaScript;

    @Override
    public void prepareSuite() throws DatabaseRuleException {
        runScript(createSchemaScript);
    }

    @Override
    public void cleanupSuite() throws DatabaseRuleException {
        runScript(deleteSchemaScript);
    }

    @Override
    public void prepareTest() throws DatabaseRuleException {
        runScript(updateSchemaScript);
    }

    @Override
    public void cleanupTest() throws DatabaseRuleException {
        runScript(cleanupSchemaScript);
    }

    private void runScript(String scriptResource) throws DatabaseRuleException {
        loadDriver();
        // Note: Both the connection and the reader should be closed automatically as they implement 'AutoClosable'
        try (Connection connection = getConnection(url, userName, password);
             Reader reader = getResourceAsReader(scriptResource);
        ) {
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            scriptRunner.setLogWriter(null);
            scriptRunner.setErrorLogWriter(null);
            scriptRunner.runScript(reader);
            connection.commit();
        } catch (SQLException | IOException e) {
            throw new DatabaseRuleException(String.format("Failed running script %s", scriptResource), e);
        }
    }

    private void loadDriver() throws DatabaseRuleException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new DatabaseRuleException(String.format("Driver '%s' not found", driver), e);
        }
    }

    @Override
    public DatabaseRule setDriver(String driver) {
        this.driver = driver;
        return this;
    }

    @Override
    public DatabaseRule setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
    public DatabaseRule setUsername(String userName) {
        this.userName = userName;
        return this;
    }

    @Override
    public DatabaseRule setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public DatabaseRule setCreateSchemaScript(String createSchemaScript) {
        this.createSchemaScript = createSchemaScript;
        return this;
    }

    @Override
    public DatabaseRule setUpdateSchemaScript(String updateSchemaScript) {
        this.updateSchemaScript = updateSchemaScript;
        return this;
    }

    @Override
    public DatabaseRule setCleanupSchemaScript(String cleanupSchemaScript) {
        this.cleanupSchemaScript = cleanupSchemaScript;
        return this;
    }

    @Override
    public DatabaseRule setDeleteSchemaScript(String deleteSchemaScript) {
        this.deleteSchemaScript = deleteSchemaScript;
        return this;
    }

    @Override
    public String toString() {
        return "DefaultDatabaseRule{" +
                "driver='" + driver + '\'' +
                ", url='" + url + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", createSchemaScript='" + createSchemaScript + '\'' +
                ", updateSchemaScript='" + updateSchemaScript + '\'' +
                ", cleanupSchemaScript='" + cleanupSchemaScript + '\'' +
                ", deleteSchemaScript='" + deleteSchemaScript + '\'' +
                '}';
    }
}
