package org.jitest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to specify a collection of 'Integration Test Configuration' needed by an 'Integration Test' run using
 * the custom JUnit runner {@link org.jitest.runner.ITestClassRunner}. When multiple configuration(s) are specified,
 * the custom runner will run every test case as many times as configurations are specified, using one specific
 * configuration per run.
 *
 * @author Francisco Cortes Gamez.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Configurations {

    /**
     * The class type containing the 'Integration Test Configuration'.
     *
     * @return The class type containing the 'Integration Test Configuration'.
     */
    Class<?>[] value();
}
