package org.jitest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to give a meaningful name to a configuration.
 *
 * @author Francisco Cortes Gamez.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ConfigurationName {

    /**
     * Custom configuration name. It could be useful to specify more readable names to a configuration.
     *
     * @return The custom configuration name.
     */
    String value();
}
