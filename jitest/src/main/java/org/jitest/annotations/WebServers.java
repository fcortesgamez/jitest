package org.jitest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to declare multiple web servers in an integration test.
 *
 * @author Francisco Cortes Gamez.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface WebServers {

    /**
     * The collection of 'Web Servers' that need to be started.
     *
     * @return The collection of 'Web Servers' that need to be started.
     */
    WebServer[] value();
}
