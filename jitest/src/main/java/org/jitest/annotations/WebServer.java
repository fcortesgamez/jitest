package org.jitest.annotations;

import java.lang.annotation.*;

/**
 * Annotation to declare that a Web Server needs to be up and running.
 *
 * @author Francisco Cortes Gamez.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Repeatable(WebServers.class)
public @interface WebServer {

    /**
     * The host name to connect to start/stop the web server.
     *
     * @return The host name to connect to start/stop the web server.
     */
    String host();

    /**
     * The user name to use to connect to the host containing web server.
     *
     * @return The user name to use to connect to the host containing web server.
     */
    String userName();

    /**
     * The password to use to connect to the host containing web server.
     *
     * @return The password to use to connect to the host containing web server.
     */
    String password();

    /**
     * The command to use to start the web server in the given host.
     *
     * @return The command to use to start the web server in the given host.
     */
    String startCommand();

    /**
     * The command to use to stop the web server in the given host.
     *
     * @return The command to use to stop the web server in the given host.
     */
    String stopCommand();
}
