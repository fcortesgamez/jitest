package org.jitest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to declare multiple databases in an integration test.
 *
 * @author Francisco Cortes Gamez.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Databases {

    /**
     * The database connections.
     *
     * @return The database connections.
     */
    Database[] value();
}