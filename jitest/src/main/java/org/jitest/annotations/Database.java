package org.jitest.annotations;

import java.lang.annotation.*;

/**
 * Annotation to declare the use of a database in an integration test.
 *
 * @author Francisco Cortes Gamez.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Repeatable(Databases.class)
public @interface Database {

    /**
     * The JDBC driver to use.
     *
     * @return The JDBC driver to use.
     */
    String driver();

    /**
     * The connection URL.
     *
     * @return The connection URL.
     */
    String url();

    /**
     * The connection user name.
     *
     * @return The connection user name.
     */
    String userName();

    /**
     * The connection password.
     *
     * @return The connection password.
     */
    String password();

    /**
     * The SQL script to use to create the schema.
     *
     * @return The SQL script to use to create the schema.
     */
    String createSchemaScript();

    /**
     * The SQL script to populate the database.
     *
     * @return The SQL script to populate the database.
     */
    String updateSchemaScript();

    /**
     * The SQL script to clean up the database.
     *
     * @return The SQL script to clean up the database.
     */
    String cleanupSchemaScript();

    /**
     * The SQL script to delete the database.
     *
     * @return The SQL script to delete the database.
     */
    String deleteSchemaScript();
}