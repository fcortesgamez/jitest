package org.jitest.runner;

import org.jitest.annotations.ConfigurationName;
import org.jitest.annotations.Database;
import org.jitest.core.JITestException;
import org.jitest.db.DatabaseRule;
import org.jitest.db.impl.DefaultDatabaseRule;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.runner.Description.createSuiteDescription;

/**
 * Custom runner used to enforce proper suite and tests preparation.
 *
 * @author Francisco Cortes Gamez.
 */
public class ITestClassRunner extends BlockJUnit4ClassRunner {

    // The configuration name.
    final String configName;

    // The list of databases that need to be in place.
    final List<DatabaseRule> databaseRules = new ArrayList<>();

    /**
     * Create an instance of {@code ITestClassRunner} with the given configuration class.
     *
     * @param clazz       The test class.
     * @param configClass The given configuration class.
     * @throws InitializationError
     */
    public ITestClassRunner(Class<?> clazz, Class<?> configClass) throws InitializationError {
        super(clazz);
        this.configName = getConfigurationName(configClass);
        this.databaseRules.addAll(extractDatabases(configClass));
    }

    @Override
    public void run(RunNotifier notifier) {
        try {
            if (prepareSuite(notifier)) {
                super.run(notifier);
            }
        } finally {
            cleanupSuite(notifier);
        }
    }

    @Override
    protected void runChild(FrameworkMethod method, RunNotifier notifier) {
        try {
            if (prepareTest(method, notifier)) {
                super.runChild(method, notifier);
            }
        } finally {
            cleanupTest(method, notifier);
        }
    }

    private boolean prepareSuite(RunNotifier notifier) {
        return prepareSuiteDatabaseRules(notifier);
    }

    private boolean prepareSuiteDatabaseRules(RunNotifier notifier) {
        for (DatabaseRule databaseRule : databaseRules) {
            try {
                databaseRule.prepareSuite();
            } catch (JITestException e) {
                notifier.fireTestFailure(new Failure(createSuiteDescription(getName()), e));
            }
        }
        return true;
    }

    private boolean cleanupSuite(RunNotifier notifier) {
        return cleanupSuiteDatabaseRules(notifier);
    }

    private boolean cleanupSuiteDatabaseRules(RunNotifier notifier) {
        for (DatabaseRule databaseRule : databaseRules) {
            try {
                databaseRule.cleanupSuite();
            } catch (JITestException e) {
                notifier.fireTestFailure(new Failure(createSuiteDescription(getName()), e));
                return false;
            }
        }
        return true;
    }

    private boolean prepareTest(FrameworkMethod method, RunNotifier notifier) {
        return prepareTestDatabaseRules(method, notifier);
    }

    private boolean prepareTestDatabaseRules(FrameworkMethod method, RunNotifier notifier) {
        for (DatabaseRule databaseRule : databaseRules) {
            try {
                databaseRule.prepareTest();
            } catch (JITestException e) {
                notifier.fireTestFailure(new Failure(describeChild(method), e));
                return false;
            }
        }
        return true;
    }

    private boolean cleanupTest(FrameworkMethod method, RunNotifier notifier) {
        return cleanupTestDatabaseRules(method, notifier);
    }

    private boolean cleanupTestDatabaseRules(FrameworkMethod method, RunNotifier notifier) {
        for (DatabaseRule databaseRule : databaseRules) {
            try {
                databaseRule.cleanupTest();
            } catch (JITestException e) {
                notifier.fireTestFailure(new Failure(describeChild(method), e));
                return false;
            }
        }
        return true;
    }

    @Override
    protected String getName() {
        return String.format("%s [%s]", super.getName(), configName);
    }

    @Override
    protected String testName(FrameworkMethod method) {
        return String.format("%s [%s]", method.getName(), configName);
    }

    private String getConfigurationName(Class<?> configClass) {
        ConfigurationName configNameAnno = configClass.getAnnotation(ConfigurationName.class);
        return configNameAnno != null ? configNameAnno.value() : configClass.getName();
    }

    private List<DatabaseRule> extractDatabases(Class<?> configClass) throws InitializationError {
        return asList(configClass.getAnnotations())
                .stream()
                .filter(anno -> anno instanceof Database)
                .map(anno -> processDatabaseAnnotation((Database) anno))
                .filter(anno -> anno != null)
                .collect(toList());
    }

    private DatabaseRule processDatabaseAnnotation(Database databaseAnno) {
        return new DefaultDatabaseRule()
                .setDriver(databaseAnno.driver())
                .setUrl(databaseAnno.url())
                .setUsername(databaseAnno.userName())
                .setPassword(databaseAnno.password())
                .setCreateSchemaScript(databaseAnno.createSchemaScript())
                .setUpdateSchemaScript(databaseAnno.updateSchemaScript())
                .setCleanupSchemaScript(databaseAnno.cleanupSchemaScript())
                .setDeleteSchemaScript(databaseAnno.deleteSchemaScript());
    }
}
