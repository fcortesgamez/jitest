package org.jitest.runner;

import org.jitest.annotations.Configurations;
import org.junit.runner.Runner;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom runner used to enforce proper suite.
 *
 * @author Francisco Cortes Gamez.
 */
public class ITestSuite extends Suite {

    /**
     * Create an instance of {@code ITestSuite} for the given test class.
     *
     * @param clazz The given test class.
     * @throws InitializationError
     */
    public ITestSuite(Class<?> clazz) throws InitializationError {
        super(clazz, createRunners(clazz));
    }

    // Create a list of custom runners for every supplied configuration class specified in the annotation
    // '@Configurations'.
    private static List<Runner> createRunners(Class<?> clazz) throws InitializationError {
        final List<Runner> runners = new ArrayList<>();
        Configurations configsAnno = (Configurations) clazz.getAnnotation(Configurations.class);
        if (configsAnno == null) {
            throw new InitializationError("Expected annotation of type '" + configsAnno.annotationType() + "' not found");
        }
        final Class<?>[] configs = configsAnno.value();
        if (configs == null || configs.length < 1) {
            throw new InitializationError("At least one Configuration is expected");
        }
        for (Class<?> config : configs) {
            runners.add(new ITestClassRunner(clazz, config));
        }
        return runners;
    }


}
