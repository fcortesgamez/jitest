package org.jitest.core;

/**
 * Generic interface with methods to ensure a resource is properly initialized and cleaned up before and after a suite
 * and/or test is run.
 *
 * @author Francisco Cortes Gamez.
 */
public interface JITestRule {

    /**
     * Prepares the test suite context before the test(s) can be actually run.
     *
     * @throws JITestException
     */
    void prepareSuite() throws JITestException;

    /**
     * Clean up the test suite context after the test(s) have been run.
     *
     * @throws JITestException
     */
    void cleanupSuite() throws JITestException;

    /**
     * Prepares the test context before the test(s) can be actually run.
     *
     * @throws JITestException
     */
    void prepareTest() throws JITestException;

    /**
     * Clean up the test context after the test(s) have been run.
     *
     * @throws JITestException
     */
    void cleanupTest() throws JITestException;
}
