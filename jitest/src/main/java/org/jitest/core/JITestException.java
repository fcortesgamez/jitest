package org.jitest.core;

/**
 * Exception to throw when something goes wrong setting up or cleaning up either the test suite or test(s).
 *
 * @author Francisco Cortes Gamez.
 */
public class JITestException extends Exception {

    public JITestException(String message) {
        super(message);
    }

    public JITestException(String message, Throwable cause) {
        super(message, cause);
    }

    public JITestException(Throwable cause) {
        super(cause);
    }
}
